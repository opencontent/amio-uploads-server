# Hasura Upload Server

Simple node application for uploading attachments to an s3 bucket

## Installation

To run the application it is sufficient to install all packages through npm and run the command

    npm start


To run the application in the docker environment, launch the commands

    docker-compose build
    docker-compose up -d

## Configuration

For the correct functioning of the application it is necessary to define the following environment variables

|   Name                    |   Description             |
|---------------------------|---------------------------|
|   S3_ACCESS_KEY_ID        |   Aws access key id       |
|   S3_SECRET_ACCESS_KEY    |   Aws secret access key   |
|   S3_ENDPOINT             |   Aws s3 endpoint         |
|   S3_BUCKET               |   Aws s3 bucket           |
|   S3_REGION               |   Aws region              |

## Requests

The application provides two http endpoints:

### [POST] `/storage/upload`

#### Headers:

    x-path: s3 path where the file shoud be stored (mandatory header)
    Content-Type: multipart/form-data

All the files to be uploaded to the s3 bucket must necessarily be contained in an array named `files`

#### Response:

For each uploaded file an object like the following will be returned

        {
            "originalname": "File.png",
            "mimetype": "image/png",
            "encoding": "7bit",
            "key": "<x-path>/<uuid-key>"
        }

where `x-path` corresponds to the path specified in the request header and `uuid-key` the unique name of the file in the bucket.

#### CLI Example

```
http --form POST https://yourserver/storage/upload files@./file.png x-path:/test/file.png
```


### [GET] `/storage/file/<key>`

The `key` returned from the previous request will be required for file download
    
