const express = require('express');
const Joi = require('joi');
const Boom = require('boom');
const uuidv4 = require('uuid');
const multer = require('multer');
const multerS3 = require('multer-s3');
const AWS = require('aws-sdk');

const {
  S3_ACCESS_KEY_ID,
  S3_SECRET_ACCESS_KEY,
  S3_ENDPOINT,
  S3_BUCKET,
  S3_REGION,
} = require('./config');


const router = express.Router();

const s3 = new AWS.S3({
  accessKeyId: S3_ACCESS_KEY_ID,
  secretAccessKey: S3_SECRET_ACCESS_KEY,
  endpoint: S3_ENDPOINT,
  s3ForcePathStyle: true,
  signatureVersion: 'v4',
  region: S3_REGION
})

const upload = multer({
  storage: multerS3({
    s3: s3,
    bucket: S3_BUCKET,
    metadata: (req, file, cb) => {
      cb(null, {
        originalname: file.originalname,
      });
    },
    contentType: function (req, file, cb) {
      cb(null, file.mimetype);
    },
    key: function (req, file, cb) {
      //generate unique file names to be saved on the server
      const uuid = uuidv4.v4();
      let ext = file.originalname.substring(file.originalname.lastIndexOf('.'), file.originalname.length);
      const key = `${req.s3_key_prefix}${uuid}${ext}`;

      req.saved_files.push({
        originalname: file.originalname,
        mimetype: file.mimetype,
        encoding: file.encoding,
        key,
      });

      cb(null, key);
    },
  }),
});

const upload_auth = (req, res, next) => {
  //path to where the file will be uploaded to
  try {
    req.s3_key_prefix = req.headers['x-path'].replace(/^\/+/g, ''); // remove first '/' if any
  } catch (e) {
    return next(Boom.badImplementation('x-path header incorrect'));
  }


  // all uploaded files gets pushed in to this array
  // this array is returned back to the client once all uploads are
  // completed
  req.saved_files = [];

  next();
};

router.post('/upload', upload_auth, upload.array('files', 50), function (req, res) {
  res.json(req.saved_files);
})


router.get('/file/*', (req, res, next) => {
  const key = `${req.params[0]}`;
  const params = {
    Bucket: S3_BUCKET,
    Key: key,
  };

  console.log(params)

  s3.headObject(params, function (err, data) {
    if (err) {
      if (err.code === 'NotFound') {
        return next(Boom.notFound());
      }
      return next(Boom.badImplementation("Unable to retrieve file"));
    }

    const stream = s3.getObject(params).createReadStream();

    //forward errors
    stream.on('error', function error(err) {
      console.error(err);
      return next(Boom.badImplementation());
    });

    //Add the content type to the responde (it's not propagated from the S3 SDK)
    res.set('Content-Type', data.ContentType);
    res.set('Content-Length', data.ContentLength);
    res.set('Last-Modified', data.LastModified);
    res.set('Content-Disposition', `inline: filename="${data.Metadata.originalname}"`);

    stream.on('end', () => {
      console.log("served by Amazon S3: " + key);
    });

    //Pipe the s3 object to the response
    stream.pipe(res);
  })
})


module.exports = router;
