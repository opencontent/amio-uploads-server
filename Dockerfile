FROM node:10-alpine
COPY package*.json /app/
WORKDIR /app
RUN npm install --only-prod
COPY . .
EXPOSE 3000
CMD [ "npm", "start" ]
